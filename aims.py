import numpy as np

if __name__=='__main__':   
    print "Welcome to the AIMS module"

def std(x):
    xbar = np.mean(x)
    devsum=0.0
    for i in range(len(x)):
        dev2 = (x[i] - xbar)**2
        devsum += dev2
    return np.sqrt(devsum/len(x))

def avg_range(lst):
    files=[]
    ranges=[]
    for location in lst:
        files.append(open(location))
    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))
    return sum(ranges)/len(ranges)
